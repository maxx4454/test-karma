import os
import paramiko
from scp import SCPClient

class SSHManager:
    def __init__(self):
        self.ssh_client = paramiko.SSHClient()
        self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    def connect(self, hostname, username):
        """Connect to the server using the default SSH key."""
        # Try to use the default RSA or DSA key
        key_path_rsa = os.path.expanduser('~/.ssh/id_rsa')
        key_path_dsa = os.path.expanduser('~/.ssh/id_dsa')
        if os.path.exists(key_path_rsa):
            rsa_key = paramiko.RSAKey(filename=key_path_rsa)
            self.ssh_client.connect(hostname=hostname, username=username, pkey=rsa_key)
        elif os.path.exists(key_path_dsa):
            dsa_key = paramiko.DSSKey(filename=key_path_dsa)
            self.ssh_client.connect(hostname=hostname, username=username, pkey=dsa_key)
        else:
            raise ValueError("No RSA or DSA private key found")

    def send_file(self, local_path, remote_path):
        with SCPClient(self.ssh_client.get_transport()) as scp:
            scp.put(local_path, remote_path)

    def execute_command(self, command):
        stdin, stdout, stderr = self.ssh_client.exec_command(command)
        return stdout.read()

    def get_public_ssh(self):
        """Retrieve the public SSH key."""
        public_key_path = os.path.expanduser('~/.ssh/id_rsa.pub')
        if os.path.exists(public_key_path):
            with open(public_key_path, 'r') as key_file:
                return key_file.read()
        else:
            raise ValueError("Public SSH key not found")

    def close_connection(self):
        self.ssh_client.close()
