from sshmanager import SSHManager
from polling import PollingManager
from yandexcloud import YandexCloudManager
from config import Config

import time

if __name__ == "__main__":
    try:
        auth = Config("./auth.yaml")
        instance = Config("./instance.yaml")
        docker_cfg = Config("./docker.yaml")
        image_name = docker_cfg.get('image_name')

        ssh_manager = SSHManager()
        ssh_pub = ssh_manager.get_public_ssh()
        instance.add_ssh(ssh_pub)

        manager = YandexCloudManager(auth.get("oauth_token"), auth.get("folder_id"))

        instance_id = manager.create(**instance.get_dict())

        print("instance_id", instance_id)

        time.sleep(90)

        ip = manager.get_instance_ipv4(instance_id)
        print("ip", ip)
        ssh_manager.connect(ip, instance.get('user_name'))
        out = ssh_manager.execute_command("sudo snap install docker")
        print(out)
        ssh_manager.send_file(docker_cfg.get("path"), "/home/"+instance.get('user_name'))
        out = ssh_manager.execute_command("sudo docker load < *.tar")
        print(out)

        out = ssh_manager.execute_command(f"sudo docker run -d --name {image_name} {docker_cfg.get('image_id')}")
        print(out)

        polling_manager = PollingManager(ssh_manager)
        polling_manager.poll_container_status(image_name, docker_cfg.get("interval"))

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        manager.delete(instance_id)
        print("end")
