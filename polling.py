import time

class PollingManager:
    def __init__(self, ssh_manager):
        self.ssh_manager = ssh_manager

    def poll_container_status(self, container_name, interval=300):
        """Опрашивать статус Docker контейнера."""
        check_status_command = f"sudo docker ps -a -f name={container_name} --format '{{{{.Status}}}}'"

        while True:
            output = self.ssh_manager.execute_command(check_status_command).strip()
            
            if isinstance(output, bytes):
                output = output.decode('utf-8')

            if not output:
                print(f"Container {container_name} not found.")
                break

            print(f"Status of container {container_name}: {output}")

            if "Exited" in output or "stopped" in output.lower():
                print(f"Container {container_name} has stopped.")
                break

            time.sleep(interval)
