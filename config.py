import yaml

GB = 1024 * 1024 * 1024

class Config:
    def __init__(self, path):
        with open(path, 'r') as file:
            self.config = yaml.safe_load(file)
            if 'memory' in self.config:
                self.config['memory'] *= GB
            if 'disk_size' in self.config:
                self.config['disk_size'] *= GB

    def get(self, key, default=None):
        return self.config.get(key, default)
    
    def add_ssh(self, ssh_pub):
        self.config['public_ssh_key'] = ssh_pub
    
    def get_dict(self):
        return self.config