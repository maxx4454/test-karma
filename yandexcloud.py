from yandex.cloud.compute.v1.instance_service_pb2 import StopInstanceRequest, StartInstanceRequest, GetInstanceRequest, DeleteInstanceRequest
from yandex.cloud.compute.v1.instance_service_pb2_grpc import InstanceServiceStub
import requests
import json
import grpc
import time

class YandexCloudManager:
    def __init__(self, oauth_token, folder_id):
        self.oauth_token = oauth_token
        self.folder_id = folder_id
        self.channel = grpc.secure_channel('compute.api.cloud.yandex.net:443', grpc.ssl_channel_credentials())
        self.compute_stub = InstanceServiceStub(self.channel)
        self.iam_token = self.get_iam_token()
        self.base_url = 'https://compute.api.cloud.yandex.net/compute/v1/instances'


    def get_iam_token(self):
        url = "https://iam.api.cloud.yandex.net/iam/v1/tokens"
        headers = {"Content-Type": "application/json"}
        payload = {"yandexPassportOauthToken": self.oauth_token}
        
        response = requests.post(url, json=payload, headers=headers)
        if response.status_code == 200:
            token = response.json()["iamToken"]
            if token is None:
                raise Exception("IAM token is not available. Cannot start the instance.")
            return token

        else:
            raise Exception("IAM token is not available. Cannot start the instance.")

    def create(self, instance_name, memory, cores, core_fraction, gpus, zone_id, platform_id, image_id, subnet_id, disk_size, user_name, public_ssh_key):
        headers = {
            'Authorization': f'Bearer {self.iam_token}',
            'Content-Type': 'application/json'
        }

        user_data = '''#cloud-config
datasource:
 Ec2:
  strict_id: false
ssh_pwauth: no
users:
- name: {user_name}
  sudo: ALL=(ALL) NOPASSWD:ALL
  shell: /bin/bash
  ssh_authorized_keys:
  - {public_ssh_key}
'''.format(user_name=user_name, public_ssh_key=public_ssh_key)

        create_data = {
            'folderId': self.folder_id,
            'name': instance_name,
            'zoneId': zone_id,
            'platformId': platform_id,
            'resourcesSpec': {
                'memory': memory,
                'cores': cores,
                'coreFraction': core_fraction,
                'gpus': gpus
            },
            'bootDiskSpec': {
                'diskSpec': {
                    'size': disk_size,
                    'typeId': 'network-ssd',
                    'imageId': image_id
                }
            },
            'networkInterfaceSpecs': [
                {
                    'subnetId': subnet_id,
                    'primaryV4AddressSpec': {
                        'oneToOneNatSpec': {
                            'ipVersion': 'IPV4'
                        }
                    }
                }
            ],
            'metadata': {
                'install-unified-agent': '0',
                'ssh-keys': '{user_name}:{public_ssh_key}'.format(user_name=user_name, public_ssh_key=public_ssh_key),
                'user-data': user_data,
            },
        }

        response = requests.post(self.base_url, headers=headers, data=json.dumps(create_data))
        if response.status_code == 200:
            return response.json()['metadata']['instanceId']
        else:
            raise Exception(f'Failed to create instance: {response.text}')


        
    def list_instances(self):
        url = f"{self.base_url}?folderId={self.folder_id}"
        headers = {
            'Authorization': f'Bearer {self.iam_token}',
            'Content-Type': 'application/json'
        }

        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            instances = response.json().get('instances', [])
            if instances:
                # Assuming there is only one instance in the folder
                return instances[0]['id']
            else:
                raise Exception("No instances found in the folder.")
        else:
            raise Exception(f'Failed to list instances: {response.text}')


    def start(self, instance_id):
        request = StartInstanceRequest(instance_id=instance_id)
        self.compute_stub.Start(request, metadata=(('authorization', 'Bearer ' + self.iam_token),))

    def get_instance_ipv4(self, instance_id):
        url = f"{self.base_url}/{instance_id}"
        headers = {
            'Authorization': f'Bearer {self.iam_token}',
            'Content-Type': 'application/json'
        }

        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            instance = response.json()
            network_interfaces = instance.get('networkInterfaces', [])
            if network_interfaces:
                # Extracting the external IPv4 address from the network interfaces
                ipv4_address = network_interfaces[0].get('primaryV4Address', {}).get('oneToOneNat', {}).get('address', None)
                if ipv4_address:
                    return ipv4_address
                else:
                    raise Exception("External IPv4 address not found for the instance.")
            else:
                raise Exception("No network interfaces found for the instance.")
        else:
            raise Exception(f'Failed to retrieve instance information: {response.text}')


    def stop(self, instance_id):
        """
        Stop an instance in Yandex Cloud.
        :param instance_id: The ID of the instance to stop.
        """
        stop_request = StopInstanceRequest(instance_id=instance_id)
        self.compute_stub.Stop(stop_request, metadata=(('authorization', 'Bearer ' + self.iam_token),))

    def delete(self, instance_id):
        """
        Delete an instance in Yandex Cloud.
        :param instance_id: The ID of the instance to delete.
        """
        delete_request = DeleteInstanceRequest(instance_id=instance_id)
        self.compute_stub.Delete(delete_request, metadata=(('authorization', 'Bearer ' + self.iam_token),))
